# crossbeam-simple-oauth2

## Overview

This is a simple Node.js-based web application that demonstrates how you might interact with the Crossbeam REST API.

Please note that this code is provided as-is, and is _not_ intended to be run as a production service. It is simply example code to help with your integration process.

## Prerequisites

- [Node LTS](https://nodejs.org/en/)
- Add `https://localhost:3001/authorize/callback` to your Crossbeam Application `Callback URL`
    - Note that you can change the port using `PORT=3333` or some other number in the `.env` file below. If you do, make sure the ports match on the `Callback URL`

## Instructions

1. Clone this repo
1. Create a `.env` file at the root of the repo (see below for further details)
1. `npm i`
1. `node .`
1. Browse to `https://localhost:3001/authorize`
1. Authenticate to Crossbeam with your credentials
1. Observe the resulting access_token. Use against any of the endpoints on the Crossbeam REST API.

## .env File

The .env file defines your configuration for the REST API. It follows this format:

```
CLIENT_ID=your-client-id-here
CLIENT_SECRET=your-client-secret-here
BASE_AUTH_URL=https://auth.crossbeam.com
AUDIENCE=https://api.getcrossbeam.com
SCOPE=openid read:reports read:populations read:partnerships read:threads offline_access
```

### Notes

- `BASE_AUTH_URL` and `AUDIENCE` should be the same as above
- `SCOPE` should be the scopes you want to grant the token you will receive (see https://developers.crossbeam.com for details)
