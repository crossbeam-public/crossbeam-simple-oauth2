require('dotenv').config()

const config = {
  client: {
    id: process.env.CLIENT_ID,
    secret: process.env.CLIENT_SECRET
  },
  auth: {
    tokenHost: process.env.BASE_AUTH_URL,
    authorizePath: `/authorize?audience=${process.env.AUDIENCE}&response_type=code&` 
  }
}
const { AuthorizationCode } = require('simple-oauth2')
const client = new AuthorizationCode(config)

const https = require('https')
const express = require('express')
const fs = require('fs')
const app = express()
const port = process.env.PORT || 3001

app.get('/authorize', (req, res) => {
  const authorizationUri = client.authorizeURL({
    redirect_uri: `https://localhost:${port}/authorize/callback`,
    scope: process.env.SCOPE
  })
  res.redirect(authorizationUri)
})

app.get('/authorize/callback', async (req, res) => {
  const { code } = req.query;
    const options = {
      code,
      redirect_uri: `https://localhost:${port}/authorize/callback`
    };

    try {
      const accessToken = await client.getToken(options);
      return res.status(200).json(accessToken);
    } catch (error) {
      console.error('Access Token Error', error.message);
      return res.status(500).json('Authentication failed');
    }
})

https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app)
.listen(port, function () {
  console.log(`crossbeam-simple-oauth2 running on https://localhost:${port}`)
})
